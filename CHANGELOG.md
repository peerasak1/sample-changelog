# Changelog

<a name="next"></a>
## next

### Added

- 🔊 Add log in viewcontroller [[de04b6a](https://gitlab.com/peerasak1/sample-changelog/commit/de04b6a08663dd2e82592a85bc9278759629ffd2)]
- ✨ Add new log in AppDelegate [[73f9a8a](https://gitlab.com/peerasak1/sample-changelog/commit/73f9a8a8b5be04f9ddb62f8a53fd516a753fab7d)]
- ✨ Add new log in AppDelegate [[2c74b40](https://gitlab.com/peerasak1/sample-changelog/commit/2c74b40ec955558551f1962de94bf2ee68d5ce1c)]

### Fixed

- 🐛 Fix some bug [[dbcc45e](https://gitlab.com/peerasak1/sample-changelog/commit/dbcc45ebedff991b54a9666773c90409ae2a9284)]
- 🐛 Add some bug [[68f997c](https://gitlab.com/peerasak1/sample-changelog/commit/68f997c4341a3af3aa42a2c4a8e11d1a7f0c4e4e)]

### Miscellaneous

-  Merge branch &#x27;master&#x27; of gitlab.com:peerasak1/sample-changelog [[acd09fe](https://gitlab.com/peerasak1/sample-changelog/commit/acd09fe878826f4201c9a1e765f97b001762b01e)]


<a name="1.0.3"></a>
## 1.0.3 (2020-08-24)

### Added

- ✨ Add action to call &#x60;helloWorld&#x60; function when start [[3c8f962](https://gitlab.com/peerasak1/sample-changelog/commit/3c8f962b5a19525baff14a271262eb57ece463f7)]

### Miscellaneous

-  👷 Add gitmoji-changelog rc [[7a42b41](https://gitlab.com/peerasak1/sample-changelog/commit/7a42b410724ca0af8920dc959f2200e2c60b090b)]


<a name="1.0.2"></a>
## 1.0.2 (2020-08-24)

### Changed

- 💬 Update text &quot;Hello world&quot; to &quot;Hello Ookbee&quot; [[c8d3ccc](https://gitlab.com/peerasak1/sample-changelog/commit/c8d3ccc79f0214e9ea7124c03d84ae007942528a)]

### Fixed

- ✏️ Fix wrong typo &quot;Hello Ookbee&quot; [[b40e161](https://gitlab.com/peerasak1/sample-changelog/commit/b40e161f99e94af59c8f73c18ffa247a19335dc0)]


<a name="1.0.1"></a>
## 1.0.1 (2020-08-24)

### Added

- ✨ Add HelloWorld function [[e4277eb](https://gitlab.com/peerasak1/sample-changelog/commit/e4277eb071cfc384cc65e6e921621f9e54b15618)]

### Miscellaneous

-  Initial Commit [[a13d90b](https://gitlab.com/peerasak1/sample-changelog/commit/a13d90bfb5a8aacdd7faeebc4c431a0a62273f0e)]


